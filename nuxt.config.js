require('dotenv').config()
const head = require('./head')

module.exports = {
  /*
  ** Headers of the page
  */
  head,
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'exact-is-active',
  },
  transition: 'page',
  css: [
    '~assets/sass/global.sass',
    '~assets/sass/layouts/transition.scss',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  plugins: [
    '~plugins/buefy',
    // { src: '~plugins/polyfills', ssr: false },
  ],

  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    postcss: {
      plugins: {
        // eslint-disable-next-line
        // 'postcss-object-fit-images': require('postcss-object-fit-images'),
        'postcss-cssnext': {
          features: {
            customProperties: false,
          },
        },
      },
    },
    /*
    ** Run ESLint on save
    */
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
  meta: false,

  modules: [
    '@nuxtjs/workbox',

    // Recomendados
    // ['@nuxtjs/google-analytics', { ua: 'UA-' }],
  ],
}
